#!/bin/bash

while read -r line; do
    city=""
    romaja=""
    latitude=""
    longitude=""
    if [[ $line =~ (.+?)\ (.+?)\ (.+?),(.+) ]]; then
        city="${BASH_REMATCH[1]}"
        romaja="${BASH_REMATCH[2]}"
        latitude="${BASH_REMATCH[3]}"
        longitude="${BASH_REMATCH[4]}"
    fi

    echo "      <city>"
    if [[ ${romaja#*-} == "gun" ]]; then
        echo "        <!-- A county in South Korea, \"$city\" in Hangul -->"
    elif [[ $romaja == "Seoul" ]]; then
        echo "        <!-- The capital of South Korea, \"$city\" in Hangul -->"
    else
        echo "        <!-- A city in South Korea, \"$city\" in Hangul -->"
    fi
    echo "        <_name>${romaja%-*}</_name>"
    echo "        <coordinates>$latitude $longitude</coordinates>"
    echo "      </city>"
done < result.txt
