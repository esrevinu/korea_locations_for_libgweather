#!/bin/bash

while read -r line; do
    city=""
    city2=""
    romaja=""
    if [[ $line =~ ([^\(\)]+)(\((.+)\))*,(.+) ]]; then
        city="${BASH_REMATCH[1]}"
        city2="${BASH_REMATCH[3]}"
        romaja="${BASH_REMATCH[4]}"
    fi

    if [[ -z $city2 ]]; then
        coord=$(grep ",$city,,,," coordinates.txt | cut -d, -f6-7)
    else
        coord=$(grep "$city2,$city,,,," coordinates.txt | cut -d, -f6-7)
    fi

    if [[ -z $city2 ]]; then
        echo "$city $romaja $coord"
    else
        echo "$city($city2) $romaja $coord"
    fi
done < cities.txt
